'''
Created on 10 nov. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''

import log
import config
import utils
import uart
import time
import sms
import at

def Init():
    utils.ModemInit()
    utils.CpinInit()
    utils.CregInit()
    utils.CsqInit()



def SendMeteringData():

    res, humidity, temperature = uart.ReadAm2305()

    if ( res != 0 ):
        return

    # send SMS to all phones from the 'phones' configuration file
    for phone in config.cfg.phones:
        sms.SendSms( phone, "Res:" + str(res) + " H:" + humidity + "% T: " + temperature + "C" )

    utils.SocketInit( 'AT#SCFG=' + config.cfg.get( "SCFG" ) + '\r', \
        'AT#SCFGEXT=' + config.cfg.get( "SCFGEXT" ) + '\r' )

    at.sendATDefault( 'AT#SCFGEXT2=1,1,0,0,0,2\r', 'OK' )
    utils.ContextInit()
    
    context = utils.CheckContext()
    if( context != '1' ):
        log.Log('Activation GPRS context')
        utils.ActivateContext()
        
        socket = utils.CheckSocket('1')
        if( socket not in ['1'] ):
            log.Log('Trying to open a socket #1 (data)')
            try:
                sn = utils.GetImei()
                utils.Connect('1', config.cfg.get("server_addr"), config.cfg.get("server_port"), 3)
                utils.SendMDM( "#{}\n#{}#{}\n#{}#{}\n##".format( sn, sn + "01", str(humidity), sn + "02", str(temperature) ) )
                time.sleep(2)
                utils.ReceiveMDM()
            except Exception, e:
                log.Log(e)
                #reboot()
    
    return 0    