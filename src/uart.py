'''
Created on 1 nov. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''

import sys
import time

import SER2
import MDM
import MDM2

import at
import log
import config
import sms

UartBuffRx = ''
UartBuffTx = ''

debug_flg = 0
uart_speed = ''
uart_dps = ''

def Init():
    global uart_speed
    global uart_dps
    uart_speed = config.cfg.get("uart_speed")
    uart_dps = config.cfg.get("uart_dps")
    SER2.set_speed( uart_speed, uart_dps )
    time.sleep(1)
    MDM.send('at#portcfg=3\r',0)
    time.sleep(1)
    MDM.send('at#spiopen=3,1,1\r',0)
    time.sleep(1)
    MDM.send('at#spiclose=3\r',0)
    time.sleep(1)
    

def UartRx():
    data = ''
    while( 1 ):
        rcv = SER2.read()
        if(len(rcv) > 0):
            data = data + rcv
        else:
            break
            
    if( len(data) > 0):
        log.Log( 'UART Rx: ' + str( len( data ) ) + ' bytes: ', data, 1 )

    return data


def UartTxStr( s ):
    if( len(s) > 0):
#        log.Log( 'UART Tx: ' + str( len( s ) ) + ' bytes: ', s, 1 )
        SER2.send( s )
        
def UartTx ( b ):
    SER2.send( b )
 
        
def BuffInHex( s ):
    hexstring = ''
    for i in s:
        hexstring = hexstring + '%.02x' % ord(i)
        
    return hexstring


def UartTxStrInHex( s ):
    for i in s:
        mychar = ord(i) #ord('a')
        #hexstring = format(i, '#04x')
        hexstring = '%.02x' % mychar
#        SER.send(hex(ord(i)))
        SER2.send(hexstring)
        SER2.send( ' ' )


def UartTxEndLine():
#    SER.send( '\r' )
    SER2.send( '\n' )
    
def ReadAm2305():
    magic_byte = 0x7e
    
    SER2.send( chr( magic_byte ) )
    
    start = time.time()
    data = ''
    while ( time.time() - start < 3 ):
        x = SER2.read()
        if ( len ( x ) > 0 ):
            data = data + x

    if ( len( data ) != 10 ):
        return ( -1, '', '' )
    
    hhum = ( int( data[0:2],16 ) )&0xff
    lhum = ( int( data[2:4],16 ) )&0xff
    htem = ( int( data[4:6],16 ) )&0xff
    ltem = ( int( data[6:8],16 ) )&0xff
    crc = ( int( data[8:10],16 ) )&0xff
    
    calc_crc = ( hhum + lhum + htem + ltem )&0xff
        
    # verify crc
    if ( calc_crc != crc ):
        return ( -2, '', '' )
     
    hum = str( ( ( ( hhum << 8 ) + lhum )&0xffff ) * 0.1 )
    
    if ( htem & 0x80 ):
        tem = '-' + str( ( ( ( htem << 8 ) + ltem )&0x7fff ) * 0.1 )
    else:
        tem = '+' + str( ( ( ( htem << 8 ) + ltem )&0xffff ) * 0.1 )
    
    return ( 0, hum, tem )

    
def Test():
    while(1):
        #print UartRx()
        UartTxStr("sadfsdf")
        sms.SendSms("+79506280447", "Hi from telit ue-868 internal")
        time.sleep(1)