'''
Created on 30 oct. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''
import MOD
import utils

def WatchdogInit():
#    Log("Watchdog init")
    MOD.watchdogEnable( 300 )
    
def WatchdogReset():
#    Log("Watchdog reset")
    MOD.watchdogReset()