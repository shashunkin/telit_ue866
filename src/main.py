'''
Created on 20 oct. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''

import time
import at
import log
import config
import sms
import date_time
import utils
import uart
import relay
import tcp

global need_reboot


def MainInit():
    
#    SER.set_speed('9600')
    
#    while( 1 == 1 ):
#        SER.send("Test\t")
#        time.sleep(1)
    
    # init starting mode
    utils.InitMode()
  
    # init at interface
    at.Init()
    
    # disable echo
    utils.DisableEcho()
    
    # read config
    config.Init()
    
    log.Log( "Script started [ " + config.cfg.get("version") + " ]" )
    
    # init watchdog
    utils.WatchdogInit()
    
    # reset watchdog
    utils.WatchdogReset()
    
    # init power saving
    utils.InitPowerSaving()
    
    # relay init
    relay.Init()
    
    #date_time init 
    date_time.Init()
    
    # uart init
    uart.Init()
    
    #read accessible numbers
    config.cfg.readPhones()
    
    log.Log( "Imei: " +  at.GetImei() )

#    sms.SendSms("+79506280447", "from telit")
    
    tcp.Init()
    


def MainLoop():
    
   
    try:
        while(1):
            
            #reset watchdog
            utils.WatchdogReset()
            
            # sync date & time
            date_time.checkSyncDateTime()
            
            # check sms
            sms.CheckSms()
            
            # send data
            tcp.SendMeteringData()
            
            # enable power saving
            utils.EnablePowerSaving()
            
            # sleeping & updating watchdog
            sleep_minutes = int( config.cfg.get( 'meter_interval' ) )
            cnt_sleep_minutes = 0
            while( cnt_sleep_minutes < sleep_minutes ):
                cnt_sleep_minutes = cnt_sleep_minutes + 1
                time.sleep( 60 )
                utils.WatchdogReset()
            
            # disable power saving
            utils.DisablePowerSaving()
            
    except Exception, e:
        log.Log( e )
        utils.Reboot()
    


MainInit()
MainLoop()


