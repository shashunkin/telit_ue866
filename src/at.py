'''
Created on 20 oct. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''

import MDM
import MDM2
import sys
import time

import log

def Init( ):
    pass
#    MDM.send('at#cmuxscr=0\r',0)
#    MDM.send('at#startmodescr=2\r', 0)
#1,10\r',0)
#SER.set_speed('115200', '8N1')
  

def sendAT(atcom, atres, trys = 1, timeout = 1, csd = 1, needlog = 1 ):
    mdm_timeout = 1
    s = MDM2.read(  )
    result = -2

    while(1):
        if log.debug_flg:
            if ( needlog ):
                logMsg = atcom
                log.Log( 'SendAT() send: ' + logMsg.replace("\n", "\\n").replace("\r", "\\r") )
        s = ''
        MDM2.send(atcom, mdm_timeout)
        timer = start = time.time() + timeout
        while(1):
            s = s + MDM2.read()
            if(s.find(atres) != -1):
                result = 0
                break
            if(s.find('ERROR') != -1):
                result = -1
                break
            if(time.time() > timer):
                break
        if log.debug_flg:
            if ( needlog ):
                logMsg = s[2:]
                log.Log( 'SendAT() recv: ' + logMsg.replace("\n", "\\n").replace("\r", "\\r") )
        trys = trys - 1
        if((trys <= 0) or (result == 0)):
            break
            
        time.sleep(1)

    return (result, s)
    
def sendATData( atcom, atres, trys = 1, timeout = 1, csd = 1, needlog = 1 ):
    mdm_timeout = 1
    s = MDM.read( )
    result = -2

    while(1):
        if log.debug_flg:
            if ( needlog ):
                logMsg = atcom
                log.Log( 'SendATData() send: ' + logMsg.replace("\n", "\\n").replace("\r", "\\r") )
        s = ''
        MDM.send(atcom, mdm_timeout)
        timer = time.time() + timeout
        while(1):
            s = s + MDM.read()
            if(s.find(atres) != -1):
                result = 0
                break
            if(s.find('ERROR') != -1):
                result = -1
                break
            if(time.time() > timer):
                break
        if log.debug_flg:
             if ( needlog ):
                logMsg = s[2:]
                log.Log( 'SendATData() recv: ' + logMsg.replace("\n", "\\n").replace("\r", "\\r") )
        trys = trys - 1
        if((trys <= 0) or (result == 0)):
            break
        time.sleep(1)

    return (result, s)
    
def sendATDefault(request, response):
    at_timeout = 50
    a, s = sendAT(request, response, 1, at_timeout)
    if (a < 0):
        raise Exception, 'ERROR. sendAT(): ' + request + '#' + response

            
def GetImei( ):
    a, s = sendAT('AT+CGSN\r', 'OK', 1, 10, 0, 0)
    log.Log( "Imei: " + s)
    if( a < 0 ):
        log.Log( "ERROR. getImei() failed")
        return "0"
        
    imei = s.strip()[ 0:15 ]
    return imei


def setIOvalue(GPIOnumber, value):
  global MDM
  if MDM.mdmser.getCD() == False:
    MDM.mdmser.write('AT#GPIO=')
    MDM.mdmser.write(str(GPIOnumber))
    MDM.mdmser.write(',')
    MDM.mdmser.write(str(value))
    MDM.mdmser.write(',1\r')
    data = ''
    timeMax = time.time() + 5.0
    while data.find('OK\r') == -1 and data.find('ERROR\r') == -1 and time.time() < timeMax:
      data = data +  MDM.mdmser.read(100)
    if data.find('OK\r') != -1:
      result = 1
    else:
      result = -1
  else:
    #print 'dummy setIOvalue('+str(GPIOnumber)+','+str(value)+')'
    result = 1
  return result


def getGPIO( gpio ):
    a, s = sendAT('AT#GPIO=' + str(gpio) + ',2\r', 'OK', 1, 10, 0, 0)
    log.Log( "Imei: " + s)
    if( a < 0 ):
        log.Log( "ERROR. getGPIO() failed")
        return -1
     
    commaPos = s.find(',')
    if ( commaPos == -1 ):
        #print ('commapos=' + commaPos)
        return -1

    #print int(s[commaPos+1])
    return int(s[commaPos+1])


def getGPIOvalue(GPIOnumber):
    MDM.mdmser.write('AT#GPIO=')
    MDM.mdmser.write(str(GPIOnumber))
    MDM.mdmser.write(',2\r')
    data = ''
    timeMax = time.time() + 5.0
    while data.find('OK\r') == -1 and data.find('ERROR\r') == -1 and time.time() < timeMax:
      data = data +  MDM.mdmser.read(100)
    if data.find('OK\r') != -1:
      commaPos = data.find(',')
      if ( commaPos == -1 ):
       result = -1
       #print "!!!!!!bad get gpio"
       return result
      stat = data[commaPos+1]
      result = int(stat)
    else:
      result = -1
      
    return result
