'''
Created on 20 oct. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''

import time

import GPIO
import MDM

import at
import log
import config

gpio_relay1 = ''
gpio_relay2 = ''


def Init():
    global gpio_relay1
    global gpio_relay2
    gpio_relay1 = config.cfg.get( "gpio_relay1" )
    gpio_relay2 = config.cfg.get( "gpio_relay2" )
#    print gpio_relay1
#    print gpio_relay2

    if ( ( int( gpio_relay1 ) < 1 ) or ( int( gpio_relay1 ) > 7 ) ):
        log.Log( "Bad gpio 'gpio_relay1' in config" )
        return
    
    if ( ( int( gpio_relay2 ) < 1 ) or ( int( gpio_relay2 ) > 7 ) ):
        log.Log( "Bad gpio 'gpio_relay2' in config" )
        return
    
    GPIO.setIOdir( gpio_relay1, 0, 1 )
    GPIO.setIOdir( gpio_relay2, 0, 1 )


# 1. gpio number
# 2. state: 'on' | 'off'
def RelaySetState( relay_num, state ):
    
    if ( ( int( relay_num ) < 1 ) or ( int( relay_num ) > 7 ) ):
        log.Log( "Bad gpio number in RelaySetState()" )
        return
    
    if ( state == "on" ):
        GPIO.setIOvalue( relay_num, 1 )        
    if ( state == "off" ):
        GPIO.setIOvalue( relay_num, 0 )


# return: 'on' | 'off'
def RelayGetState( relay_num ):
    
    if ( ( relay_num < 1 ) or ( relay_num > 7 ) ):
        log.Log( "Bad gpio number in RelayGetState()" )
        return
    
    #state = GPIO.getIOvalue( relay_num )
    state = at.getGPIO( relay_num )
    
#    print ("State=" + str(state))
    
    if ( state == 0 ):
        return 'off'        
    elif ( state == 1 ):
        return 'on'
    else:
        return "gpio error"
    
def TestRelays():
#    print "relay1:"
    log.Log( "Relay1: " + RelayGetState( gpio_relay1) )
    RelaySetState( gpio_relay1 , 'on')
    log.Log( "Relay1: " + RelayGetState( gpio_relay1) )
    RelaySetState( gpio_relay1 , 'off')
    log.Log( "Relay1: " + RelayGetState( gpio_relay1) )
    
#    print "relay2:"
    log.Log( "Relay2: " + RelayGetState( gpio_relay2) )
    RelaySetState( gpio_relay2 , 'on')
    log.Log( "Relay2: " + RelayGetState( gpio_relay2) )
    RelaySetState( gpio_relay2 , 'off')
    log.Log( "Relay2: " + RelayGetState( gpio_relay2) )