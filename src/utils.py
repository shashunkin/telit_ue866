'''
Created on 1 nov. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''

import sys
import time
import re
from datetime import datetime, timedelta

import MDM
import MDM2
import MOD

import log
import at
import config
import tcp

def ms_delay(ms):
    start = datetime.now()

    end = start + timedelta( milliseconds = ms )
    
    while( 1 ):
        delta = ( datetime.now() - start ).microseconds/1000
       
        if ( delta > ms):
            break


def GetImei( ):
    a, s = at.sendAT('AT+CGSN\r', 'OK', 1, 10, 0, 0)
    log.Log( "Imei: " + s)
    if( a < 0 ):
        log.Log( "ERROR. getImei() failed")
        return "0"
        
    imei = s.strip()[ 0:15 ]
    return imei


def GetBalance( ):
    a, s = at.sendAT('AT+CUSD=1,"' + config.cfg.get("sim_ussd") + '",15\r', '+CUSD', 1, 40)
    log.Log( "Balance: " + s )
    if( a < 0 ):
        log.Log( "ERROR. GetBalance() failed")
        return "0"
        
    MDM2.read()
    
    list_bal = re.findall(r'[+-]?\d*\.\d+', s)
    
    return list_bal[0]     


def ModemInit():
    at.sendATDefault('ATE0\r', 'OK')
    at.sendATDefault('AT\\R0\r', 'OK')
    at.sendATDefault('AT#ENHRST=2,' + '1440' + '\r', 'OK')
    at.sendATDefault('AT+CMGF=1\r', 'OK')
    log.Log('ModemInit() passed OK')


def CpinInit():
    at_timeout = 50
    at.sendAT('AT+CMEE=2\r', 'OK', 1, at_timeout)
    at.sendAT('AT+CPIN=' + config.cfg.get("sim_pin") + '\r', 'OK', 1, at_timeout)


def CsqInit():
    at_timeout = 20
    a, s = at.sendAT('AT+CSQ\r', 'OK', 3, at_timeout)
    if (a < 0):
        raise Exception, 'ERROR. CsqInit() failed'
    a_i = s.split(chr(13))
    a_i_p = a_i[1].find('+CSQ')
    log.Log('CsqInit() passed OK ' + a_i[1][a_i_p:])
    return a_i[1][a_i_p+5:]

def CregInit():
    at_timeout = 20
    a, s = at.sendAT('AT+CREG?\r', '+CREG: 0,1', 10, at_timeout)
    if (a < 0):
        raise Exception, 'ERROR. CregInit failed'
    log.Log('CregInit passed OK')


def SocketInit(scfg, scfgext):
    at.sendATDefault(scfg, 'OK')
    at.sendATDefault(scfgext, 'OK')
    log.Log("SocketInit() passed OK")

def ContextInit():
    at.sendATDefault('AT+CGDCONT=1,"IP","' + config.cfg.get("gprs_apn") + '"\r', 'OK')
    log.Log("ContextInit() passed OK")

   
def CheckContext():
    timeout = 20
    a, s = at.sendAT('AT#SGACT?\r', 'OK', 1, timeout, 0, 0 )
    d = s.find('#SGACT:')
    if (d != -1):
        return(s[d + 10])
    return '0' 

def ActivateContext():
    timeout = 40
    a, s = at.sendAT('AT#SGACT=1,0\r', 'OK', 1, timeout)
    #AT#SGACT=1,0 
    if (a == 0):
        a, s = at.sendAT('AT#SGACT=1,1,"' + '","' + '"\r', 'OK', 1, timeout)
        if (a == 0):
            str_IP = s.split(chr(13))
            i_str_IP = str_IP[1].find(': ')
            My_IP = str_IP[1][i_str_IP + 2:]
            log.Log('GPRS CONTEXT activated ... OK  IP: ' + My_IP)
            return My_IP

def Connect(socket, ip, port, trys):
    log.Log("Connect(): " + 'Socket: ' + socket + ' connected to: ' + ip + ':' + port + ' ...')
    timeout = 30
    if(socket == '1'):
        a, s = at.sendATData('AT#SD=' + socket + ',0,' + port + ',"' + ip + '",0,1,0\r', 'CONNECT', trys, timeout)
    else:
        a, s = at.sendAT('AT#SD=' + socket + ',0,' + port + ',"' + ip + '",0,1,1\r', 'OK', trys, timeout)
    if (a < 0):
        raise Exception, 'ERROR. TCP connection on socket ' + socket + ' failed'
    log.Log('Socket ' + socket + ' connected to ' + ip + ':' + port)

    
def CheckSocket(socket):
    timeout = 1
    a, s = at.sendAT('AT#SS=' + socket + '\r', 'OK', 10, timeout, 0, 0 )
    d = s.find('#SS:')
    if (d != -1):
        return(s[d + 7])
    raise Exception, 'ERROR. CheckSocket ' + socket + ' failed'


def GetBuffer(size):
    data = ''
    if(len(buffer) > size):
        data = buffer[0:size]
        buffer = buffer[size:]
    else:
        data = buffer
        buffer = ''
    return data


def BuffInHex( s ):
    hexstring = ''
    for i in s:
        hexstring = hexstring + '%.02x' % ord(i)
        
    return hexstring



def StrInHex( s ):
    hexstring = ''
    
    for i in s:
        mychar = ord(i) #ord('a')
        #hexstring = format(i, '#04x')
        hexstring = hexstring + '%.c' % mychar
        #print mychar
#        SER.send(hex(ord(i)))
    return hexstring


def ReceiveMDM():
    data = ''
    size = 512
    while(1):
        rcv = MDM.read()
        if( len(rcv) > 0 ):
            data = data + rcv
            if( len(data) > size):
                break
        else:
            break
            
    if( len(data)  > 0):
        #data = GetBuffer(size)
        #if(data.find('NO CARRIER') != -1):
        #    raise Exception, '"NO CARRIER" on socket #1'
        #if debug_flg:
        log.Log('TCP Rx: ' + str( len( data ) ) + ' bytes: ' + StrInHex(data).replace("\n", "\\n").replace("\r", "\\r") )
        #log.Log( "\tHEX: " + BuffInHex( data ) )
        
        
    return data

def SendMDM(data):
    if ( len ( data ) > 0 ) :
        MDM.send(data, 50)
        #if debug_flg:
        log.Log('TCP Tx: ' + str( len( data ) ) + ' bytes: ' + StrInHex(data).replace("\n", "\\n").replace("\r", "\\r") )


def Receive(socket = '1'):
    res_string = ''
    log.Log('Call Receive() ...')
    a, s = at.sendAT('AT#SRECV=' + socket + ',1500\r', 'OK')
    f_srecv = s.find('#SRECV')
    if (f_srecv == -1):
        return ''
    len_srecv = len(s)
    i_srecv = 9
    while (1):
        i_srecv = i_srecv + 1
        if (f_srecv + i_srecv >= len_srecv):
            return ''
        code_char = ord(s[f_srecv + i_srecv])
        if (code_char == 13):
            break
    start_int = f_srecv + 10
    end_int = f_srecv + i_srecv
    string1 = s[start_int:end_int]
    q_data = int(s[start_int:end_int])
    res_string = s[end_int + 2:end_int + 2 + q_data]
    if (q_data > 0):
        log.Log('Data received from socket #' + socket + ': ' + str(q_data) + ' bytes')
    return res_string


def Reboot():
    
    log.Log("Will be reboot!")
    
    reboot_timeout = int( config.cfg.get( 'timeout_before_reboot' ) )
                          
    while( 1 ):
        MDM.send( 'AT#REBOOT\r', 2 )
        time.sleep( reboot_timeout )
        MDM.send('AT#ENHRST=1,0\r', 2 )
        time.sleep( reboot_timeout )

        
    
def InitMode():
    MDM.send('at#startmodescr=2\r', 0)

    
def InitPowerSaving():
    MDM.send("AT+CFUN=5\r", 5)


def EnablePowerSaving():
    MDM.setDTR( 0 )
    time.sleep( 2 )


def DisablePowerSaving():
    MDM.setDTR( 1 )
    time.sleep( 2 )


def DisableEcho():
    at_timeout = 20
    a, s = at.sendAT('ATE0\r', 'OK', 10, at_timeout)
    a, s = at.sendAT('AT&K0\r', 'OK', 10, at_timeout)
    

def WatchdogInit():
    MOD.watchdogEnable( int(config.cfg.get( 'watchdog' ) ) )

    
def WatchdogReset():
    MOD.watchdogReset()