'''
Created on 31 oct. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''

import MDM
import sys
import time
import at
import log
import config
import utils
import date_time

server_daytime = ''
server_daytime_port = ''
time_zone = ''
last_sync_time = "00:00"

#from datetime import datetime, timedelta
#usleep = lambda x: time.sleep(x/1000.0)
    

def Init():
    global server_daytime
    global server_daytime_port
    global time_zone
    server_daytime = config.cfg.get("server_daytime")
    server_daytime_port = config.cfg.get("server_daytime_port")
    time_zone = config.cfg.get("time_zone")


def watchDog(watchStr, timeout):
    data = ""
    timer_start = time.time()
    while (((data.find(watchStr) == -1) \
            or (MDM.getDCD() != 0)) \
           and ( ( time.time() - timer_start ) < timeout)):
        data = data + MDM.read()
        
    data = data.replace("CONNECT", "")
    data = data.replace("NO CARRIER", "")
    data = data.strip()
    
    if not data:
        log.Log( "No read bytes!" )
        return []
    
    dateTimeList = data.split(" ")
    log.Log( dateTimeList )
    
    return dateTimeList

def syncDateTime():
    #at.sendATDefault('AT+CGDCONT=1, "IP","internet.tele2.ru"\r', 'OK')
    #am2305.ms_delay(50)
    #MDM.send('AT#SCFG=1,1,300,90,600,50\r',10)
    #am2305.ms_delay(50)
    #MDM.send('AT#SGACT = 1,1\r',10)
    #am2305.ms_delay(50)
    log.Log( server_daytime )
    mdmCommand="AT#SD=1,0," + server_daytime_port +"," + server_daytime +",255,0\r"
    MDM.send(mdmCommand,10)
    MODbufferFlush=MDM.read()
    dateTimeList=watchDog("NO CARRIER", 5)
    MDM.send('AT#SH=1\r',10)
    
    if not dateTimeList:
        log.Log( "Bad request! Exit ..." )
        return
    
    if(len(dateTimeList) < 2):
        return
    
    yyMMdd, timeStamp = dateTimeList[1:3]
#    print yyMMdd, timeStamp
    yy,MM,dd = yyMMdd.split("-")
#    print yy,MM,dd

    cclk='"%s/%s/%s,%s%s"' % (yy,MM,dd,timeStamp,time_zone)
    mdmCommand='AT+CCLK=%s\r' % cclk
    at.sendATDefault(mdmCommand, 'OK')
#    log.Log( mdmCommand )


def getDateTime():
    date_time=""
    a, s = at.sendAT('AT+CCLK?\r', 'OK', 1, 10, 0, 0)
    log.Log( "GetDateTime: " + s)
    if( a < 0 ):
        log.Log( "ERROR. getImei() failed")
        return "0"
        
    date_time = str((s.split(" "))[1:])[3:23]
    #date_time = date_time.replace('\"', "").replace('\r', "").replace('\n', "").replace('OK', "")
    
    return date_time

def checkSyncDateTime():
    syncDateTime()

 
def testDateTime():
    log.Log( 'Before: ' + date_time.getDateTime() )
    
    context = utils.CheckContext()
    if( context != '1' ):
        log.Log('Activation GPRS context')
        utils.ActivateContext()
    
    syncDateTime()
    
    log.Log( 'After: ' + date_time.getDateTime() )
    