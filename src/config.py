'''
Created on 20 oct. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''

import sys
import time

import MDM
import MDM2

import log

cfg = ''
imei = ''
need_reboot = 0

def Init():
    global cfg
    cfg = Config()
    cfg.read()

class Config:
    def __init__(self):
        self.config = {}
        
        # list of accessible phones numbers
        self.phones = []
        
        self.supported_r_keys = [
            'version',
            'watchdog',
            'sync_time',
            'sync_balance_time',
            'meter_interval',
            'sim_pin',
            'gprs_apn',
            'gprs_login',
            'gprs_passwd',
            'time_zone',
            'min_balance',
            'server_daytime',
            'server_daytime_port',
            'server_addr', 
            'server_port', 
            'timeout_on_start',
            'timeout_before_reboot',
            'gpio_relay1=5',
            'gpio_relay2=6',
            'gpio_status=7',
            'uart_speed',
            'uart_dps',
            'gpio_relay1',
            'gpio_relay2',
            'SCFG',
            'SCFGEXT'
            ]
        
        # may be overloaded by a sms
        self.supported_w_keys = [
            'watchdog',
            'sync_time',
            'sync_balance_time',
            'meter_interval',
            'sim_pin',
            'gprs_apn',
            'gprs_login',
            'gprs_passwd',
            'time_zone',
            'min_balance',
            'server_daytime',
            'server_daytime_port',
            'server_addr', 
            'server_port', 
            'timeout_on_start',
            'timeout_before_reboot'
            ]
        
    def get(self, k):
        log.Log("Call config get():" + self.config[k])
        return self.config[k]
    
    def set(self, k, v):
        log.Log("Call config set(): " + k + " : " + v)
        self.config[k] = v
        

    def checkKey( self, k ):
        if k in self.config.keys():
            log.Log("Call config checkKey(): " + k + " - true")
            return 1
        else:
            log.Log("Call config checkKey(): " + k + " - false")
            return 0
            
    def read(self):
        try:
            fh = open("config", "r")
            try:
                lines = fh.readlines()
                
                for l in lines:
                    if ( len( l ) > 0 ):
                        kv = []
                        l = l.replace(" ", "").replace("\t", "")
                        kv = l.split('=')
                        if ( len(kv) == 2 ):
                            kv = l.strip().split('=')
                            log.Log("Read() - \tkey:" + kv[0] + "\tvalue: " + kv[1] )
                            if ( kv[0] in self.supported_r_keys ):
                                self.config[kv[0]] = kv[1]
                            else:
                                log.Log("Unsupported key: " + kv[0] )
                            
            finally:
                fh.close()
        except IOError:
            log.Log( "Config file does not exist!" )
#            reboot()

    def write(self):
        try:
            fh = open("config", "w")
            try:
                lines = []
                for k in self.config.keys():
                    if ( k in self.supported_w_keys ) :
                        log.Log("Supported: " + k )    
                        log.Log("Write() - \tkey:" + k + "\tvalue: " + self.config[k] )
                        lines.append(k + '=' + self.config[k] + '\n')
                    else:
                        log.Log("Unsupported: " + k )
                fh.writelines(lines)
            finally:
                fh.close()
        except IOError:
            log.Log( "Config file does not exist!" )
#            reboot()

    def readPhones(self):
        try:
            fh = open("phones", "r")
            try:
                lines = fh.readlines()

                for l in lines:
                    if ( len( l ) > 0 ):
                        l = l.replace(" ", "").replace("\t", "")
                        log.Log("ReadPhones() : " + l )
                        self.phones.append(l)
                            
            finally:
                fh.close()
        except IOError:
            log.Log( "Config file does not exist!" )
#            reboot()
    
