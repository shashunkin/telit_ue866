'''
Created on 20 oct. 2017 y.

@author: Denis Shashunkin
@email: d.shashunkin@yandex.ru
'''

import re
import MDM
import MDM2
import log
import at
import config
import relay
import date_time
import utils
import uart


def SendSms( phone, text ) :
    log.Log( "Send sms: num: " + phone + " sms: " + text )
    
#    MDM.send('AT+CMGF=1\r', 2)
#    MDM.receive( 5 )

    cmd = 'AT+CMGS="' + phone + '"\r'
    log.Log( "Cmd: " + cmd )
    a, s = at.sendAT(cmd, '>', 1, 20 )
    d = s.find('>')
    if (d != -1):
        at.sendAT( text + chr(26), 'OK', 1, 30 )
        
    log.Log( "Ending send sms" )
    

def ParseSms( s ) :
    global imei
    global modem_state
    global connection_state
    global     need_reboot

    log.Log( "Parse sms: " + s )

    sms_phone_list = s.split(",")
    
    if ( len ( sms_phone_list ) > 0 ) :
        log.Log( "sms_phone_list () > 0 " )
        sms_phone = sms_phone_list[1][1:len(sms_phone_list[1]) - 1 ]
    else:
        log.Log( "!!!!!!!! sms_phone_list () < 0 !!!!!!!!!!!!!" )
        return
    
    log.Log( "Parse sms: NEW NUMBER" + sms_phone )
    phone = sms_phone
    
#    phone_start = s.find('+7')
#    phone = s[phone_start:phone_start+12]
    
    log.Log( "Phone: " + phone )
    
    log.Log( 'S: ' + s.replace("\n", "\\n").replace("\r", "\\r") )
    
    nsms = s.split("\r\n")
    
    if ( len(nsms) == 0 ):
        return
        
    text = nsms[2]
    log.Log( 'text: ' + text )
    
    if ( text[0] != '#' ):
        return
    
    text = text[1:]
    
    log.Log( 'text without #: ' + text )
    
    if ( len(nsms[2]) < 0 ):
        return
        
    answer_sms_text = "" # "unrecognized sms"
    preambula = ''
    
    cmd_sms = text.lower()
    cmd_sms = cmd_sms.replace( ',', ' ' ).split()
    
    #log.Log( "lower(): {}".format( cmd_sms ) )

    if cmd_sms[0] in ["info"]:
        log.Log( "find " + cmd_sms[0] )
        
        res, humidity, temperature = uart.ReadAm2305()
    
        if ( res != 0 ):
            humidity = 'error'
            temperature = 'error'
        
        answer_sms_text = \
        "SN:" + utils.GetImei() + '\n' \
        "RL1: " + \
        relay.RelayGetState( int( config.cfg.get( 'gpio_relay1' ) ) ) + '\n' \
        "RL2: " + \
        relay.RelayGetState( int( config.cfg.get( 'gpio_relay2' ) ) ) + '\n' \
        "Sensor: " + humidity + "%" + ", " + temperature + "C" + '\n' \
        "Interval: " + config.cfg.get('meter_interval') + " min" + '\n' \
        "Balance: " + utils.GetBalance() + " rub" + '\n' + \
        date_time.getDateTime() + '\r\n' 
        
#        print answer_sms_text
    
    elif cmd_sms[0] in ["set"]:
        if ( len( cmd_sms ) == 2 ):

            cmd_sms_list = cmd_sms[1].split(':')

            if ( len( cmd_sms_list ) == 2 ):
    
                value = int ( cmd_sms_list[1] )
    
                if ( cmd_sms_list[0] == 'rl1' ):
                    if ( ( value == 0 ) or \
                         ( value == 1 ) ):
                        answer_sms_text = 'Set RL1 to ' + cmd_sms_list[1] + ' : OK'
                                 
                elif ( cmd_sms_list[0] == 'rl2' ):
                    if ( ( value == 0 ) or \
                        ( value == 1 ) ):
                        answer_sms_text = 'Set RL2 to ' + cmd_sms_list[1] + ' : OK'
                                        
                elif ( cmd_sms_list[0] == 'interval' ):
                    if ( ( value >= 0 ) or \
                        ( value < 60 ) ):
                        answer_sms_text = 'Set INTERVAL to ' + cmd_sms_list[1] + ' : OK'
        
        if ( len( cmd_sms ) == 3 ):
            cmd_sms_1 = cmd_sms[1].split(':')
            cmd_sms_2 = cmd_sms[2].split(':')            
            value_1 = int ( cmd_sms_1[1] )
            value_2 = int ( cmd_sms_2[1] )
            
            if ( ( cmd_sms_1[0] == 'rl1' ) and \
                 ( cmd_sms_2[0] == 'rl2' ) ):
                if ( ( ( value_1 == 1 ) or ( value_2 == 0 ) ) and \
                     ( ( value_1 == 1 ) or ( value_2 == 0 ) ) ):
                    answer_sms_text = 'Set RL1 to ' + cmd_sms_1[1] + ' : OK' + '\n'
                    answer_sms_text = answer_sms_text +'Set RL2 to ' + cmd_sms_2[1] + ' : OK'
                     
        
#    elif cmd_sms[0] in ["get"]:
#        print "Getters"
        
    if ( len( answer_sms_text ) > 0 ):
        for phone in config.cfg.phones:
            SendSms( phone, preambula + " " + answer_sms_text )

pos = 1

def CheckSms():
    global pos
    
    if ( pos > 10 ):
        pos = 1
    
    while ( pos < 10 ):    
    #    log.Log( "reading sms ..." )
        a, s = at.sendAT('AT+CMGR=' + str( pos )  + '\r', 'OK', 5, 10, 0, 0)
    #    log.Log("Sms content:" + s )
        d = s.find('+CMGR:')
        if (d != -1):
            ParseSms( s )
    #        log.Log( "deleting sms ..." )
    #        a, s = at.sendAT('AT+CMGD=' + str( pos )  + '\r', 'OK', 5, 10, 0, 0)
        pos = pos + 1
    
    
